package service

import (
	"gitlab.com/DanielFrag/chat-sse/dto"
)

type ChatManager struct {
	clients             map[string]*ChatClient
	deliveryPacketsChan chan dto.DeliveryPacket
}

func (cm *ChatManager) handleDeliveryPackets() {
	for {
		select {
		case packet := <-cm.deliveryPacketsChan:
			if client, ok := cm.clients[packet.RecipientID]; ok {
				client.PushPacket(packet)
			}
		}
	}
}

func (cm *ChatManager) AddClient(client *ChatClient) {
	if client.GetID() != "" {
		cm.clients[client.GetID()] = client
	}
}

func (cm *ChatManager) DropClient(client *ChatClient) {
	if connectedClient, ok := cm.clients[client.GetID()]; ok {
		if client.GetSessionHash() == connectedClient.GetSessionHash() {
			delete(cm.clients, client.GetID())
		}
	}
}

func (cm *ChatManager) StartManager() {
	cm.clients = make(map[string]*ChatClient)
	cm.deliveryPacketsChan = make(chan dto.DeliveryPacket)
	go cm.handleDeliveryPackets()
}

func (cm *ChatManager) SendDeliveryPacket(packet dto.DeliveryPacket) {
	cm.deliveryPacketsChan <- packet
}
