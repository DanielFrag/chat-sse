package service

import (
	"gitlab.com/DanielFrag/chat-sse/dto"
)

type ChatClient struct {
	id          string
	sessionHash string
	Updates     chan dto.DeliveryPacket
}

func (cc *ChatClient) PushPacket(message dto.DeliveryPacket) {
	cc.Updates <- message
}

func (cc *ChatClient) GetID() string {
	return cc.id
}

func (cc *ChatClient) GetSessionHash() string {
	return cc.sessionHash
}

func (cc *ChatClient) Setup(id, hash string) {
	if cc.id == "" && id != "" {
		cc.id = id
	}
	if cc.sessionHash == "" && hash != "" {
		cc.sessionHash = hash
	}
	cc.Updates = make(chan dto.DeliveryPacket)
}

func (cc *ChatClient) Kill() {
	close(cc.Updates)
}
