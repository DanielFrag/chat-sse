package main

import (
	"fmt"
	"net/http"

	"gitlab.com/DanielFrag/chat-sse/handler"
	"gitlab.com/DanielFrag/chat-sse/service"
)

func main() {
	cm := &service.ChatManager{}
	cm.StartManager()
	fmt.Println("Started")
	getUpdates := handler.BuildGetUpdates(cm)
	postMessage := handler.BuildPostMessages(cm)
	http.HandleFunc("/updates", getUpdates)
	http.HandleFunc("/message", postMessage)
	fs := http.FileServer(http.Dir("./www/"))
	http.Handle("/", fs)
	http.ListenAndServe(":8080", nil)
}
