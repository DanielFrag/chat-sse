package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/DanielFrag/chat-sse/dto"
	"gitlab.com/DanielFrag/chat-sse/service"
	"gitlab.com/DanielFrag/chat-sse/utils"
)

func BuildGetUpdates(chatManager *service.ChatManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f, ok := w.(http.Flusher)
		if !ok {
			http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}
		currentContext := r.Context()
		client := service.ChatClient{}
		client.Setup(utils.GenerateRandomAlphaNumericString(3), utils.GenerateRandomAlphaNumericString(10))
		fmt.Println(client)
		chatManager.AddClient(&client)
		go func() {
			for {
				select {
				case <-currentContext.Done():
					chatManager.DropClient(&client)
					client.Kill()
					return
				}
			}
		}()
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Transfer-Encoding", "chunked")
		for {
			message, active := <-client.Updates
			if !active {
				break
			}
			data, _ := json.Marshal(message)
			fmt.Fprintf(w, "event: %s\nid: %s\ndata: %s\n\n", "foo", "asdf", string(data))
			f.Flush()
		}
		fmt.Println("Client droped: ", client.GetID())
	}
}

func BuildPostMessages(chatManager *service.ChatManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		body, bodyReadError := ioutil.ReadAll(r.Body)
		if bodyReadError != nil {
			http.Error(w, "Error reading body request: "+bodyReadError.Error(), http.StatusInternalServerError)
			return
		}
		var message dto.DeliveryPacket
		json.Unmarshal(body, &message)
		chatManager.SendDeliveryPacket(message)
	}
}
