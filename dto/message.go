package dto

//DeliveryPacket data struct that represents a packet to be sent to clients
type DeliveryPacket struct {
	SenderID    string `json:"sender"`
	RecipientID string `json:"recipient"`
	Message     string `json:"message"`
}
